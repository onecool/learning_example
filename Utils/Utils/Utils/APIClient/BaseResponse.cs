﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Utils.APIClient
{
    /// <summary>
    /// 返回基类
    /// </summary>
    public  class BaseResponse<T> where T : BaseResponseData, new()
    {
        public HttpWebResponse HttpWebResponse { get; set; }

        public string ResponseString { get; set; }

        public T Data { get; set; }
    }
}