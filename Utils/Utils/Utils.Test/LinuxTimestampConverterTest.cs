﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.APIClient;

namespace Utils.Test
{
    [TestClass]
    public class LinuxTimestampConverterTest
    {
        [TestMethod]
        public void DateConverterTest()
        {
            var date = new DateTime(1888,8,8,8,8,8);
            
            var testDate = new testDateClass()
            {
                TestDate = date
            };
            var converter = new LinuxTimestampConverter();
            var json=JsonConvert.SerializeObject(testDate, converter);
            testDate = JsonConvert.DeserializeObject<testDateClass>(json, converter);
            Assert.AreEqual(testDate.TestDate, date);
        }

        [TestMethod]
        public void IsoDateConverterTest()
        {
            var date = new DateTime(1888, 8, 8, 8, 8, 8);

            var testDate = new testDateClass()
            {
                TestDate = date,
                test = date
            };
            var converter = new LinuxTimestampConverter();
            var json = JsonConvert.SerializeObject(testDate, converter);
            Assert.AreEqual(json.IndexOf("1888-08-08") >= 0, true);

        }

        private class testDateClass
        {
            public DateTime TestDate { get; set; }

            public DateTime? Create { get; set; }

            [JsonConverter(typeof(IsoDateTimeConverter))]
            public DateTime test { get; set; }
        }
    }
}
