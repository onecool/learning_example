using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Acme.HotelStore.Hotels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Acme.HotelStore.Web.Pages.HotelStore
{
    [Authorize(Permissions.HotelStorePermissions.Hotel.Edit)]
    public class EditModalModel : HotelStorePageModel
    {
        [BindProperty]
        public CreateOrUpdateHotelDto Hotel { get; set; }

        [HiddenInput]
        [BindProperty(SupportsGet =true)]
        public Guid Id { get; set; }


        IHotelAppService hotelAppService;

        public EditModalModel(IHotelAppService hotelAppService)
        {
            this.hotelAppService = hotelAppService;
        }

        public async void OnGet()
        {
            var hotelDto = await hotelAppService.GetAsync(Id);
            Hotel = ObjectMapper.Map<HotelDto, CreateOrUpdateHotelDto>(hotelDto);
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await hotelAppService.UpdateAsync(Id, Hotel);
            return NoContent();
        }
    }
}
