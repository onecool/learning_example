﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;
using Volo.Abp.Domain.Entities.Auditing;

namespace Acme.HotelStore.Hotels
{
    public class Hotel:AuditedAggregateRoot<Guid>
    {
        public string HotelName { get; set; }

        public string Address { get; set; }
        

    }
}
