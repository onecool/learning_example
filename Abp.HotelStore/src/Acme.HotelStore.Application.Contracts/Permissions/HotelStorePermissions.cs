﻿using Volo.Abp.Reflection;

namespace Acme.HotelStore.Permissions
{
    public class HotelStorePermissions
    {
        public const string GroupName = "HotelStore";

        public static string[] GetAll()
        {
            return ReflectionHelper.GetPublicConstantsRecursively(typeof(HotelStorePermissions));
        }

        public static class Hotel
        {
            public const string Default = GroupName + ".Hotel";
            public const string Create = Default + ".Create";
            public const string Edit = Default + ".Edit";
            public const string Delete = Default + ".Delete";
        }
    }
}