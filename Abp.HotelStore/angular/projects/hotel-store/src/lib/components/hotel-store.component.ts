import { Component, OnInit } from '@angular/core';
import { HotelStoreService } from '../services/hotel-store.service';

@Component({
  selector: 'lib-hotel-store',
  template: ` <p>hotel-store works!</p> `,
  styles: [],
})
export class HotelStoreComponent implements OnInit {
  constructor(private service: HotelStoreService) {}

  ngOnInit(): void {
    this.service.sample().subscribe(console.log);
  }
}
