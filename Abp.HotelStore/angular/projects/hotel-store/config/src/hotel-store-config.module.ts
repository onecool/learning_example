import { ModuleWithProviders, NgModule } from '@angular/core';
import { HOTEL_STORE_ROUTE_PROVIDERS } from './providers/route.provider';

@NgModule()
export class HotelStoreConfigModule {
  static forRoot(): ModuleWithProviders<HotelStoreConfigModule> {
    return {
      ngModule: HotelStoreConfigModule,
      providers: [HOTEL_STORE_ROUTE_PROVIDERS],
    };
  }
}
