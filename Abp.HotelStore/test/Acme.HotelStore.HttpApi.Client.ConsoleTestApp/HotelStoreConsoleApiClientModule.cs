﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace Acme.HotelStore
{
    [DependsOn(
        typeof(HotelStoreHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class HotelStoreConsoleApiClientModule : AbpModule
    {
        
    }
}
