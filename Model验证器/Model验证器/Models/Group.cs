﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Model验证器.Models
{
    public class Group
    {
        [Required]
        [StringLength(16)]
        public string GroupCode { get; set; }

        [Required]
        [StringLength(16)]
        public string GroupName { get; set; }

    }
}
