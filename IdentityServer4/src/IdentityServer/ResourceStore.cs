﻿using IdentityServer4.Models;
using IdentityServer4.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer
{
    public class ResourceStore : IResourceStore
    {

        public Task<ApiResource> FindApiResourceAsync(string name)
        {
            return Task.FromResult(Config.GetApis().FirstOrDefault(r => r.Name == name));
        }

        public Task<IEnumerable<ApiResource>> FindApiResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            return Task.FromResult(Config.GetApis().Where(r => scopeNames.Contains(r.Name)));
        }

        public Task<IEnumerable<IdentityResource>> FindIdentityResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            return Task.FromResult(Config.GetIdentityResources().Where(r => scopeNames.Contains(r.Name)));
        }

        public Task<Resources> GetAllResourcesAsync()
        {
            var res = new Resources(Config.GetIdentityResources(), Config.GetApis());
            return Task.FromResult(res);
        }
    }
}
