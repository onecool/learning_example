using Microsoft.AspNetCore.Mvc;

namespace CoreMQ.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Home : ControllerBase
    {
  

        private readonly ILogger<Home> _logger;

        public Home(ILogger<Home> logger)
        {
            _logger = logger;
        }


        [HttpPost]
        public string Publish()
        {
            return "OK";
        }
    }
}