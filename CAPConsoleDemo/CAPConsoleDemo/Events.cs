﻿using DotNetCore.CAP;
using System;
using System.Threading.Tasks;

namespace CAPConsoleDemo
{
    public class Events : ICapSubscribe
    {


        [CapSubscribe("Order.Created", Group = "Group1")]   //监听Order.Created事件,并命名为Group1
        public async Task OrderCreatedEventHand(string msg)
        {
            Console.WriteLine("--Group1接收：" + msg);
        }


        [CapSubscribe("Order.Created", Group = "Group2")]   //监听Order.Created事件,并命名为Group2
        public async Task Group2(string msg)
        {
            Console.WriteLine("--Group2接收：" + msg);
        }
    }
}
