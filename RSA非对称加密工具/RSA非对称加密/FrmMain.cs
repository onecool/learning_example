﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RSA非对称加密
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }
        private void FrmMain_Load(object sender, EventArgs e)
        {
            var codes = Encoding.GetEncodings();

            CobSignEncoding.ValueMember = "Name";
            CobSignEncoding.DisplayMember = "DisplayName";
            CobSignEncoding.DataSource = codes;
            CobSignEncoding.SelectedValue = "utf-8";

            CobVerifyEncoding.ValueMember = "Name";
            CobVerifyEncoding.DisplayMember = "DisplayName";
            CobVerifyEncoding.DataSource = codes;
            CobVerifyEncoding.SelectedValue = "utf-8";

            CobEncryptEncoding.ValueMember = "Name";
            CobEncryptEncoding.DisplayMember = "DisplayName";
            CobEncryptEncoding.DataSource = codes;
            CobEncryptEncoding.SelectedValue = "utf-8";

            CobDecryptEncoding.ValueMember = "Name";
            CobDecryptEncoding.DisplayMember = "DisplayName";
            CobDecryptEncoding.DataSource = codes;
            CobDecryptEncoding.SelectedValue = "utf-8"; 

        }

        /// <summary>
        /// 创建密钥
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCreateKey_Click(object sender, EventArgs e)
        {
            var KeySize = 2048;
            for (var i = 0; i < PanKeySize.Controls.Count; i++)
            {
                var radio = PanKeySize.Controls[i] as RadioButton;
                if (radio.Checked)
                {
                    KeySize = Convert.ToInt32(radio.Text);
                    break;
                }
            }
            using (var key = RSA.Create(KeySize))
            {
                TbxPrivateKey.Text = key.ToXmlString(true);
                TbxPublicKey.Text = key.ToXmlString(false);
            }
        }
        
        /// <summary>
        /// 创建签名
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCreateSign_Click(object sender, EventArgs e)
        {
            var encode = System.Text.Encoding.GetEncoding(CobSignEncoding.SelectedValue.ToString());
            var hashAlgorithmName = RabRsa.Checked ? HashAlgorithmName.SHA1 : HashAlgorithmName.SHA256;
            var data = encode.GetBytes(TbxSignContent.Text);
            using (var key = RSA.Create())
            {
                key.FromXmlString(TbxSignKey.Text);
                var signatureBytes = key.SignData(data, hashAlgorithmName, RSASignaturePadding.Pkcs1);
                var signatureStr= Convert.ToBase64String(signatureBytes);
                TbxSignValue.Text = signatureStr;
            }
        }

        /// <summary>
        /// 验证签名
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnVerify_Click(object sender, EventArgs e)
        {
            var encode = System.Text.Encoding.GetEncoding(CobVerifyEncoding.SelectedValue.ToString());
            var hashAlgorithmName = RabVerifyRsa.Checked ? HashAlgorithmName.SHA1 : HashAlgorithmName.SHA256;
            var data = encode.GetBytes(TbxVerifyContent.Text);
            var signBytes = Convert.FromBase64String(TbxVerifySignValue.Text);
            using (var key = RSA.Create())
            {
                key.FromXmlString(TbxVerifyPublicKey.Text);
                var isVerify = key.VerifyData(data, signBytes, hashAlgorithmName, RSASignaturePadding.Pkcs1);
                MessageBox.Show(isVerify ? "验证通过" : "验证失败");
            }
        }

        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnEncrypt_Click(object sender, EventArgs e)
        {
            var encode = System.Text.Encoding.GetEncoding(CobVerifyEncoding.SelectedValue.ToString());
            var data = encode.GetBytes(TbxEncryptCntent.Text);
            using (var key = RSA.Create())
            {
                key.FromXmlString(TbxEncryptPublicKey.Text);
                var encryptBytes = key.Encrypt(data, RSAEncryptionPadding.Pkcs1);
                TbxEncryptValue.Text = Convert.ToBase64String(encryptBytes);
            }
        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDecrypt_Click(object sender, EventArgs e)
        {
            var encode = System.Text.Encoding.GetEncoding(CobDecryptEncoding.SelectedValue.ToString());
            var decryptData = Convert.FromBase64String(TbxDecryptValue.Text);
            using (var key = RSA.Create())
            {
                key.FromXmlString(TbxDecryptPrivateKey.Text);
                var dataBytes = key.Decrypt(decryptData, RSAEncryptionPadding.Pkcs1);
                TbxDecryptContent.Text = encode.GetString(dataBytes);
            }
        }

        private void LabCopyPrivatekey_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(TbxPrivateKey.Text))
            {
                Clipboard.SetText(TbxPrivateKey.Text);
            }
        }

        private void LabCopyPublicKey_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(TbxPublicKey.Text))
            {
                Clipboard.SetText(TbxPublicKey.Text);
            }
        }
    }
}
