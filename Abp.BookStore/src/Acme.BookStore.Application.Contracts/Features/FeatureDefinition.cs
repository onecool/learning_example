﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Features;
using Volo.Abp.Localization;
using Volo.Abp.Validation.StringValues;

namespace Acme.BookStore.Application.Contracts.Features
{
    public class FeatureDefinition: FeatureDefinitionProvider
    {
        public override void Define(IFeatureDefinitionContext context)
        {
            var group = context.AddGroup("BookStore");

            group.AddFeature(
                "BookStore.Books",
                defaultValue: "false",               
                valueType: new ToggleStringValueType()
            );

            group.AddFeature(
                "BookStore.Author",
                defaultValue: "false",
                valueType: new ToggleStringValueType()
            );
        }
    }
}
