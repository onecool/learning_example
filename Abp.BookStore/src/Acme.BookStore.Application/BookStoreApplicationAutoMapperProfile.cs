﻿using Acme.BookStore.Authors;
using Acme.BookStore.Books;
using AutoMapper;
using Volo.Abp.AutoMapper;

namespace Acme.BookStore
{
    public class BookStoreApplicationAutoMapperProfile : Profile
    {
        public BookStoreApplicationAutoMapperProfile()
        {
            // 已有自定义映射BookToBookDtoMapper，不再需要此语句。CreateMap<Book, BookDto>();

            CreateMap<CreateUpdateBookDto, Book>();
            CreateMap<BookDto, CreateUpdateBookDto>();
            CreateMap<Author, AuthorLookupDto>();

            CreateMap<Author, AuthorDto>();
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}
