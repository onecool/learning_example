﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.EventBus;

namespace Acme.BookStore.Authors
{
    [EventName("Acme.BookStore.Authors.AuthorNameChangeEvent")]
    public class AuthorNameChangeEvent
    {
        public Author Author { get; set; }

        public string OldName { get; set; }
    }
}
