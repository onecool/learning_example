﻿using Volo.Abp.Localization;

namespace Acme.BookStore.Localization
{
    [LocalizationResourceName("Books")]
    public class BooksResource
    {

    }
}