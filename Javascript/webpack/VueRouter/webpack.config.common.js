const webpack = require('webpack');
const path=require('path');
const yaml = require('yamljs');
const json5 = require('json5');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports={
    entry:{
        index:'./src/main.js'
    },
    output:{
        path:path.resolve(__dirname,'dist'),
        filename:'js/[name].[chunkhash].js',
        chunkFilename: 'js/[id].[chunkhash].js',
        //publicPath: '/'   //webpacek-dev-server 虚拟目录，设置浏览器访问的根地址
        clean:true    //发布时清理dist
    },
    resolve: {
        extensions: ['.js', '.vue', '.json'],//配置import时文件的默认扩展名
        alias: {
          'vue$': 'vue/dist/vue.esm.js',     //为文件指定另名，可把这理解为一个字符串常量，其它地方可以直接通过常量名访问
          '@': path.resolve(__dirname,'src'),
        }
    },
    devServer: {                    //webpack server 服务器
        static: {
           directory: path.join(__dirname, 'dist'),
        },
        port: 9010,
        compress:true
    },
    plugins:[
        new HtmlWebpackPlugin({    //自动生成index.html
           filename: 'index.html',
           template: 'index.html'
        }),
        new VueLoaderPlugin(),
        new WebpackManifestPlugin({})    //生成manifest.json映射图
    ],
    module:{
        rules:[
            {
                test: /\.vue$/i,
                include: path.resolve(__dirname, 'src'),  //仅解析src目录下的文件
                use: ['vue-loader']
            },
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                  loader: 'babel-loader',
                  options: {
                    presets: ['@babel/preset-env']
                  }
                }
            },
            {
                test: /\.css$/i,
                include: path.resolve(__dirname, 'src'),  //仅解析src目录下的文件
                use: ['style-loader','css-loader','postcss-loader']
            },
            {
                test: /\.s[ac]ss$/i,
                use:[
                      'style-loader',
                      'css-loader',
                      {
                        loader:'postcss-loader'
                      },
                      'sass-loader'
                    ]
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: 'asset/resource',
                generator: {
                    filename: 'images/[hash][ext][query]'
                }
            },
            {
                test: /\.yaml$/i,
                type: 'json',
                parser: {
                  parse: yaml.parse,
                },
            },
            {
                test: /\.json5$/i,
                type: 'json',
                parser: {
                  parse: json5.parse,
                },
            }
        ]
    },
    optimization: {
        runtimeChunk:'single', 
        splitChunks: {
            cacheGroups: {              //公共Js分离
                vendor: {    
                    name: "vendor",
                    test: /[\\/]node_modules[\\/]/,
                    chunks: "all",
                    priority: 10 // 优先级
                },
                common: {
                    name: "common",
                    test: /[\\/]commonjs[\\/]/,
                    minSize: 1024,
                    chunks: "all",
                    priority: 5
                }
            }
        }
    },
};


