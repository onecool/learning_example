// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import app from './app'
import VueRouter from 'vue-router'
import Vuex from 'vuex'

Vue.use(VueRouter);
Vue.use(Vuex);

const router = new VueRouter({
  mode:'hash',
  routes :[
    { path:'/r',redirect:'/home'},
    { path: '/home', alias:'', name:'home', component: ()=>import("./pages/home") },
    { path: '/about/:id', component: ()=>import("./pages/about") },
    { path: '/user/:username', props:true, component: ()=>import("./pages/user") },
    { path: '*', component: ()=>import("./pages/404") }
  ]
});
router.beforeEach(function(to,from,next){
    console.log(to);
    next(true);
});

const store=new Vuex.Store({
    state:{
      userName:'admin',
      password:'admin',
      realName:'管理员'
    },
    mutations:{
        chnagePassword(state,input){
          if(input.oldPassword==state.password){
            state.password=input.newPassword;
            console.log(`修改密码为：${state.password}` );
          }
       }
    }
});


/* eslint-disable no-new */
var aps=new Vue({  
  router,
  store,
  data:{},
  components: { app },
  template: '<app/>'
}).$mount('#app');