const { merge }= require('webpack-merge');
const common = require('./webpack.config.common.js');

module.exports = merge(common, {
    mode:'development'   //开发模式，有源码映射
 });