// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import app from './app'

Vue.config.productionTip = false


/* eslint-disable no-new */
var aps=new Vue({
  el: '#app',
  data:{url:''},
  components: { app },
  template: '<app :url="url"/>'
});

window.onhashchange = function () {
   var url=location.hash.substring(1);
   if(!url) url="home";
   aps.url=url;
};
window.onhashchange();