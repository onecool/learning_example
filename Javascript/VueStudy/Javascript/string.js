export default function string(str){
     this.str=str;

     this.trim=function(){
         return this.str.trim();
     }
     this.right=function(length){
        return this.str.substring(this.str.length-length);
     }
     this.left=function(length){
         return this.str.substring(0,length);
     }
     this.toString=()=>{
         return this.str;
     }
}

export function max(a,b){
    return a>b?a:b;
}
export function min(a,b){
    return a>b?b:a;
}

