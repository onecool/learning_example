﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Events
{
    public interface IEventBus
    {
        Task Push<T>(T @event) where T : IEvent;
    }
}
