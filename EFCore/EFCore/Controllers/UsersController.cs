﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EFCore.Models;
using EFCore.Repository;
using EFCore.EntityFrameworkExtensions;
using System.Linq.Expressions;
using Gridify;

namespace EFCore.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly DataContext context;

        public UsersController(DataContext context)
        {
           this.context = context;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUser()
        {
            Expression<Func<EFCore.Models.User, bool>> expression0 = u => u.UserNmae.Contains("Ad");
            Expression<Func<EFCore.Models.User, bool>> expression1 = u => u.Birthday>=new DateTime(1900,1,1);
            Expression<Func<EFCore.Models.User, bool>> expression2 = u => u.Role.Users.Count > 0;

            List<QueryEntity> list = new List<QueryEntity>
            {
                new QueryEntity
                {
                    Key = "Role.Users.Count",
                    Value = "0",
                    Operator = OperatorEnum.GreaterEqual
                },
                new QueryEntity
                {
                    Key = "Birthday",
                    Value = "1900/1/1",
                    Operator = OperatorEnum.GreaterEqual
                }
            };
            var t = context.User.ApplyFiltering("UserNmae=*Ad , (Birthday>=1900/1/1 | UserNmae=*da)");   //Role.Users.Count>=0不支持 Role.RoleName=*a 不支持
            await t.ToListAsync();
            return await context.User.Where(list).OrderBy("UserNmae desc,Role.Users.Count asc").ThenBy(r=>r.Age).ToListAsync();
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(string id)
        {
            var user = await context.User.FindAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        // PUT: api/Users/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(string id, User user)
        {
            if (id != user.UserNmae)
            {
                return BadRequest();
            }

            context.Entry(user).State = EntityState.Modified;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Users
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<User>> PostUser(User user)
        {
            context.User.Add(user);
            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UserExists(user.UserNmae))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetUser", new { id = user.UserNmae }, user);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> DeleteUser(string id)
        {
            var user = await context.User.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            context.User.Remove(user);
            await context.SaveChangesAsync();

            return user;
        }

        private bool UserExists(string id)
        {
            return context.User.Any(e => e.UserNmae == id);
        }
    }
}
