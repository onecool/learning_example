﻿--script-migration -from  20200704050736_InitialCreate -output EFCore\Repository\Sql\20200704051946_addBirthday.sql

ALTER TABLE [User] ADD [Birthday] datetime2 NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200704051946_addBirthday', N'3.0.0');

GO

