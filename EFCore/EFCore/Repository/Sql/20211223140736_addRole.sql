﻿--script-migration -from  20200704051946_addBirthday -output EFCore\Repository\Sql\20211223140736_addRole.sql

ALTER TABLE [User] ADD [RoleID] nvarchar(450) NULL;

GO

CREATE TABLE [Role] (
    [RoleID] nvarchar(450) NOT NULL,
    [RoleName] nvarchar(max) NULL,
    CONSTRAINT [PK_Role] PRIMARY KEY ([RoleID])
);

GO

CREATE INDEX [IX_User_RoleID] ON [User] ([RoleID]);

GO

ALTER TABLE [User] ADD CONSTRAINT [FK_User_Role_RoleID] FOREIGN KEY ([RoleID]) REFERENCES [Role] ([RoleID]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20211223143719_addRole', N'3.0.0');

GO

